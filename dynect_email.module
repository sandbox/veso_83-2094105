<?php

/**
 * @file
 * Enables Drupal to send email directly through DynECT.
 *
 * Overriding mail handling in Drupal to make DynECT the default
 * transport layer, requires to change the mail_system variable's
 * default value array('default-system' => 'DefaultMailSystem').
 * This module uses array('default-system' => 'DynECT Mail System').
 */


define('dynect_email_QUEUE', 'dynect_email_queue');
define('dynect_email_EMAIL_REGEX', '/^\s*(.+?)\s*<\s*([^>]+)\s*>$/');

/**
 * Implements hook_help().
 */
function dynect_email_help($path, $arg) {
  switch ($path) {
    case 'admin/help#dynect_email':
      return t('Allow for site emails to be sent through dynect_email.');
  }
}

/**
 * Implements hook_menu().
 */
function dynect_email_menu() {
  $items = array();
  $items['admin/config/services/dynect_email'] = array(
    'title' => 'dynect_email',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('dynect_email_admin_settings'),
    'access arguments' => array('administer dynect_email'),
    'description' => 'Send emails through the dynect_email transactional email service.',
    'file' => 'dynect_email.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/config/services/dynect_email/settings'] = array(
    'title' => 'Settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,
  );
  $items['admin/config/services/dynect_email/test'] = array(
    'title' => 'Send test email',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('dynect_email_test_form'),
    'access callback' => 'dynect_email_test_access',
    'description' => 'Send a test email using the dynect_email API.',
    'file' => 'dynect_email.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );

  return $items;
}

/**
 * Access callback for sending test email.
 *
 * @return bool
 *   True if current user has access to send test messages
 */
function dynect_email_test_access() {
  $a = user_access('administer dynect_email');
  $b = variable_get('dynect_email_api_key');
  return $a & !empty($b);
}

/**
 * Implements hook_permission().
 */
function dynect_email_permission() {
  return array(
    'administer dynect_email' => array(
      'title' => t('Administer dynect_email'),
      'description' => t('Perform administration tasks for the dynect_email email service.'),
      "restrict access" => TRUE,
    ),
  );
}

/**
 * Implements hook_mail().
 */
function dynect_email_mail($key, &$message, $params) {
  if ($key == 'test') {
    $message['subject'] = $params['subject'];
    $message['body'] = $params['body'];
  }
}

/**
 * Abstracts sending of messages, allowing queueing option.
 *
 * @param array $message
 *   a message array formatted for dynect_email's sending API, plus 2 additional indexes
 *   for the send_function and an array of $args, if needed by the send function
 *
 * @return bool
 *   TRUE if no exception thrown
 */
function dynect_email_mailsend($message, $function, $args = array()) {

  try {
    if (!function_exists($function)) {
      watchdog('dynect_email', 'Error sending email from %from to %to. Function %function not found.',
        array(
          '%from' => $message['from_email'],
          '%to' => $message['to'],
          '$function' => $function,
        ),
        WATCHDOG_ERROR
      );
      return FALSE;
    }

    $params = array($message) + $args;

    $results = call_user_func_array($function, $params);

    foreach ($results as $result) {
      switch ($result['status']) {
        case "error":
        case "invalid":
        case "rejected":
          $to = isset($result['email']) ? $result['email'] : 'recipient';
          $status = isset($result['status']) ? $result['status'] : 'message';
          $error_message = isset($result['message']) ? $result['message'] : 'no message';
          watchdog('dynect_email', 'Failed sending email from %from to %to. @status: @message',
            array(
              '%from' => $message['from_email'],
              '%to' => $to,
              '@status' => $status,
              '@message' => $error_message,
            ),
            WATCHDOG_ERROR
          );
          break;

        case "queued":
          watchdog('dynect_email', 'Email from %from to %to queued by dynect_email App.',
            array(
              '%from' => $message['from_email'],
              '%to' => $result['email'],
            ),
            WATCHDOG_INFO
          );
          break;

      }
    }
    return TRUE;
  }
  catch (dynect_email_Exception $e) {
    watchdog('dynect_email', 'Error sending email from %from to %to. @code: @message',
      array(
        '%from' => $message['from'],
        '%to' => $message['to'],
        '@code' => $e->getCode(),
        '@message' => $e->getMessage(),
      ),
      WATCHDOG_ERROR
    );

    return FALSE;
  }
}


/**
 * The actual function that calls the API send message.
 *
 * This is the default function used by dynect_email_mailsend
 *
 * @param $message
 *
 * @return array
 */
function dynect_email_sender_plain($message) {
  $mailer = dynect_email_get_api_object();
  return $mailer->messages_send($message);
}

/**
 * Get a dynect_email API object for communication with the dynect server.
 */
function dynect_email_get_api_object() {
  $api_key = variable_get('dynect_email_api_key', '');
  if (empty($api_key)) {
    return FALSE;
  }

  $api = new dynect_email($api_key);

  return $api;
}

/**
 * Display the names of the modules that are using Mailsystem.
 *
 * This is consistent with with Mailsystem's display. In the future, if
 * Mailsystem were to provide an API for their labeling, that should be used.
 *
 * @return array
 *   Array of all module names indexing to their "display" names,
 *   and some special items for non-module values like null, default-system,
 *   and some clarification talked onto the end of the dynect_email module's name.
 */
function dynect_email_get_module_key_names() {
  $name_array = array(
    '' => '--none--',
    'default-system' => "Site-wide default",
  );
  $descriptions = array();
  foreach (system_rebuild_module_data() as $item) {
    if ($item->status) {
      $descriptions[$item->name] = (
      empty($item->info['package'])
        ? '' : $item->info['package']
      ) . ' » ' . t('!module module', array('!module' => $item->info['name']));
    }
  }
  asort($descriptions);
  $mailsystem_settings = mailsystem_get();
  unset($mailsystem_settings['default-system']);
  foreach ($mailsystem_settings as $id => $class) {
    // Separate $id into $module and $key.
    $module = $id;
    while ($module && empty($descriptions[$module])) {
      // Remove a key from the end.
      $module = implode('_', explode('_', $module, -1));
    }
    // If an array key of the $mail_system variable is neither "default-system"
    // nor begins with a module name, then it should be unset.
    if (empty($module)) {
      // This shouldn't happen.
    }
    // Set $title to the human-readable module name.
    $title = preg_replace('/^.* » /', '', $descriptions[$module]);
    if ($key = substr($id, strlen($module) + 1)) {
      $title .= " ($key key)";
    }
    $name_array[$id] = $title;
  }

  return $name_array;
}

/**
 * Get a list of dynect_email template objects.
 *
 * @return array
 *   An of available templates with complete data or NULL if none are available.
 */
function dynect_email_get_templates() {
  // Only show the template settings if the dynect_email api can be called.
  $templates = NULL;
  try {
    $mailer = dynect_email_get_api_object();
    $templates = $mailer->templates_list();
  }
  catch (dynect_email_Exception $e) {
    drupal_set_message(t('dynect_email: %message', array('%message' => check_plain($e->getMessage()))), 'error');
    watchdog_exception('dynect_email', $e);
  }
  return $templates;
}

/**
 * Helper to return a comma delimited list of mail keys to not log content for.
 *
 * @return string
 *   a comma delimited list of mail keys
 */
function dynect_email_mail_key_blacklist() {
  return variable_get('dynect_email_mail_key_blacklist', 'user_password_reset');
}

/**
 * Helper to generate an array of recipients.
 *
 * @param mixed $to
 *   a comma delimited list of email addresses in 1 of 2 forms:
 *   user@domain.com
 *   any number of names <user@domain.com>
 *
 * @return array
 *   array of email addresses
 */
function dynect_email_get_to($to) {
  $recipients = array();
  $to_array = explode(',', $to);
  foreach ($to_array as $email) {
    if(preg_match(dynect_email_EMAIL_REGEX, $email, $matches)) {
      $recipients[] = array(
        'email' => $matches[2],
        'name' => $matches[1],
      );
    }
    else {
      $recipients[] = array('email' => $email);
    }
  }
  return $recipients;
}

/**
 * Implements hook_cron().
 */
function dynect_email_cron() {
  $queue = DrupalQueue::get(dynect_email_QUEUE, TRUE);
  $num_items = $queue->numberOfItems();

  // Allow for items left in the queue even after async processing disabled.
  if (!dynect_email_process_async() && $num_items == 0) {
    return;
  }

  // Set our count to the lesser of batch limit or queue count.
  $limit = variable_get('dynect_email_batch_limit', 50);
  $limit = ($num_items < $limit) ? $num_items : $limit;

  for ($delta = 0; $delta != $limit; $delta++) {
    // We can't test using claimItem() as part of the loop conditional,
    // or we will accidentally pop an extra item off the queue
    // and have the lease on that item for the next hour, locking it.
    $item = $queue->claimItem();
    if (!$item) {
      break;
    }

    // Send the message stored in the queue item.
    dynect_email_mailsend(
      $item->data['message'],
      $item->data['function'],
      $item->data['args']
    );

    // Remove item from queue.
    $queue->deleteItem($item);
  }
}

/**
 * Determine if mail should be processed asynchronously.
 *
 * @return bool
 *   True if asyncronous processing is enabled
 */
function dynect_email_process_async() {
  return variable_get('dynect_email_process_async', FALSE);
}

/**
 * Returns an array containing the from information for a dynect_email message.
 *
 * @return array
 *   array(
 *     'email' => 'admin@example.com',
 *     'name' => 'My Site',
 *   )
 */
function dynect_email_from() {
  $default_from = variable_get('site_mail', ini_get('sendmail_from'));
  $email = variable_get('dynect_email_from', $default_from);
  $name = variable_get('dynect_email_from_name', variable_get('site_name'));
  return array(
    'email' => $email,
    'name' => $name,
  );
}
