<?php

/*
 * @file
 * Implements dynect_email as a Drupal MailSystemInterface
 */

/**
 * Modify the drupal mail system to use dynect_email when sending emails.
 */
class dynect_emailMailSystem implements MailSystemInterface {

  /**
   * Concatenate and wrap the email body for either plain-text or HTML emails.
   *
   * @param array $message
   *   A message array, as described in hook_mail_alter().
   *
   * @return array
   *   The formatted $message.
   */
  public function format(array $message) {
    // Join the body array into one string.
    if (is_array($message['body'])) {
      $message['body'] = implode("\n\n", $message['body']);
    }
    return $message;
  }

  /**
   * Send the email message.
   *
   * @see drupal_mail()
   *
   * @param array $message
   *   A message array, as described in hook_mail_alter().
   *
   * @return bool
   *   TRUE if the mail was successfully accepted, otherwise FALSE.
   */
  public function mail(array $message) {
    // Optionally log mail keys not using dynect_email already. Helpful in
    // configuring dynect_email.
    if (variable_get('dynect_email_log_defaulted_sends')) {
      $systems = mailsystem_get();
      $registered = FALSE;
      foreach ($systems as $key => $system) {
        if ($message['id'] == $key) {
          $registered = TRUE;
        }
        if (!$registered) {
          watchdog(
            'dynect_email',
            "Module: %module Key: %key invoked dynect_email to send email because dynect_email is configured as the default mail system. Specify alternate configuration for this module & key in !mailsystem if this is not desirable.",
            array(
              '%module' => $message['module'],
              '%key' => $message['key'],
              '!mailsystem' => l(t('Mail System'), 'admin/config/system/mailsystem'),
            ),
            WATCHDOG_INFO
          );
        }
      }
    }

    $mailer = dynect_email_get_api_object();

    // Apply input format to body.
    $html = $message['body'];
    $format = variable_get('dynect_email_filter_format', '');
    if (!empty($format)) {
      $html = check_markup($message['body'], $format);
    }

    // Extract an arry of recipients.
    $to = dynect_email_get_to($message['to']);

    // Prepare headers, defaulting the reply-to to the from address since
    // dynect_email needs the from address to be configured separately.
    // Note that only Reply-To and X-* headers are allowed.
    $headers = isset($message['headers']) ? $message['headers'] : array();
    if (!empty($message['from']) && empty($headers['Reply-To'])) {
      $headers['Reply-To'] = $message['from'];
    }


    // Determine if content should be available for this message.
    $blacklisted_keys = explode(',', dynect_email_mail_key_blacklist());
    $view_content = TRUE;
    foreach ($blacklisted_keys as $key) {
      if ($message['id'] == drupal_strtolower(trim($key))) {
        $view_content = FALSE;
        break;
      }
    }

    $from = dynect_email_from();
    $dynect_email_message = array(
      'html' => $html,
      'text' => drupal_html_to_text($message['body']),
      'subject' => $message['subject'],
      'from_email' => $from['email'],
      'from_name' => $from['name'],
      'to' => $to,
      'headers' => $headers,
      'track_opens' => variable_get('dynect_email_track_opens', TRUE),
      'track_clicks' => variable_get('dynect_email_track_clicks', TRUE),
      // We're handling this with drupal_html_to_text().
      'auto_text' => FALSE,
      'url_strip_qs' => variable_get('dynect_email_url_strip_qs', FALSE),
      'bcc_address' => isset($message['bcc_email']) ? $message['bcc_email'] : NULL,
      'tags' => array($message['id']),
      'google_analytics_domains' => (variable_get('dynect_email_analytics_domains', NULL)) ? explode(',', variable_get('dynect_email_analytics_domains')) : array(),
      'google_analytics_campaign' => variable_get('dynect_email_analytics_campaign', ''),
      'view_content_link' => $view_content,
    );

    // Allow other modules to alter the dynect_email message, and sender/args.
    $dynect_email_params = array(
      'message' => $dynect_email_message,
      'function' => 'dynect_email_sender_plain',
      'args' => array(),
    );
    drupal_alter('dynect_email_mail', $dynect_email_params, $message);

    // Queue for processing during cron or send immediately.
    $status = NULL;
    if (dynect_email_process_async()) {
      $queue = DrupalQueue::get(dynect_email_QUEUE, TRUE);
      $queue->createItem($dynect_email_params);
      watchdog('dynect_email', 'Message from %from to %to queued for delivery.',
        array(
          '%from' => $from['email'],
          '%to' => $to[0]['email'],
          WATCHDOG_NOTICE
        ));
      return TRUE;
    }
    else {
      return dynect_email_mailsend($dynect_email_params['message'], $dynect_email_params['function'], $dynect_email_params['args']);
    }
  }
}
