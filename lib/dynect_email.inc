<?php

class dynect_email_Exception extends Exception {
}

class dynect_email {
  const END_POINT = 'http://emailapi.dynect.net/rest/';

  public $api;
  public $emailaddress;
  /**
   * Default to a 300 second timeout on server calls
   */
  private $timeout = 300;


  /**
   * Constructor to set internal values.
   *
   * @param string $api_key
   *   dynect_email API key.
   * @param int $timeout
   *   Server timeout.
   */
  public function __construct($api_key) {

    if (empty($api_key)) {
      throw new dynect_email_Exception('Invalid API key');
    }
    /*else{
      throw new dynect_email_Exception('API key is:'.$api_key);
    }*/
   try {
    $this->api = $api_key;
    $this->emailaddress = variable_get('dynect_email_from','');
      $response = $this->request('senders/details', array('apikey' => $api_key, 'emailaddress' => $this->emailaddress ));
      if(empty($response)){
        throw new dynect_email_Exception('the response is :' .$response);
        //throw new dynect_email_Exception('received key is NOT empty:' .$response);
        /*if ($response['apikey'] != $api_key) {
          throw new dynect_email_Exception('received key is empty:' .$response);
         // throw new dynect_email_Exception('API keys does not match:' . $response);
        }*/
      }
      $this->api = $api_key;

    }
    catch (Exception $e) {
      throw new dynect_email_Exception($e->getMessage());
    }
  }

  /**
   * Every API call uses this function to actually make the request to dynect_email's servers.
   *
   * @link http://emailapi.dynect.net/rest/
   *
   * @param string $method
   *   API method name
   * @param array $args
   *   query arguments
   * @param string $http
   *   GET or POST request type
   * @param string $output
   *   API response format (json,php,xml,yaml). json and xml are decoded into arrays automatically.
   *
   * @return array
   *   Array on success.
   */
  public function request($method, $args = array(), $http = 'GET', $output = 'json') {
    if (!isset($args['key'])) {
      $args['key'] = $this->api;
    }

    $url = self::END_POINT . "{$output}/{$method}";


  switch ($http) {
      case 'GET':
        $args['key'] = $this->api;
        $url .= '?'.'apikey='.$args['key']. '&' .'emailaddress=' .$args['emailaddress'];
        $response = drupal_http_request($url, array(
          'method' => 'GET',
        ));
        break;

      case 'POST':

          $params = array('apikey' => $this->api, 'from' => $args['from_email'], 'to' => $args['to'], 'subject' => $args['subject'], 'bodytext' => $args['text']);

		      $params = http_build_query($params, '', '&');
		      $params = str_replace('%40','@',$params);

		      $reqestedBody = $params;
		      $data = $this->execute($method,$reqestedBody,$output);

		      $arr = explode('&',$data);

		      $params = array('apikey' => $arr[0], 'from' => $arr[1], 'to' => $arr[2], 'subject' => $arr[3], 'bodytext' => $arr[4]);

		      return $params;


        break;

      default:
        throw new dynect_email_Exception('Unknown request type');
    }


  if($http=='GET'){
    $response_code = $response->code;

    if (0 == $response_code) {
      return $response->error;
    }
    $body = $response->data;

    switch ($output) {
      case 'json':
           $body = json_decode($body, TRUE);

        break;

      case 'php':
        $body = unserialize($body);
        break;
    }

    if (200 == $response_code) {

      return $body;
    }
    else {
      $message = isset($body['message']) ? $body['message'] : '';
      //throw new dynect_email_Exception('Unable to deliver your mail, wrong data entry'.$response_code);
    }


  }

  }


private function execute($method,$reqestedBody,$output)
	{
		$ch = curl_init();

		try
		{
			strtoupper($method);
      $this->executePost($ch,$reqestedBody,$method,$output);
      if(empty($method)){
					throw new dynect_email_Exception('Method is empty');
			}
			return $reqestedBody;
		}
		catch(Exception $e)
		{
			curl_close($ch);
			throw new dynect_email_Exception('Invalid arguments');
		}
		catch(Exception $e)
		{
			curl_close($ch);
			throw new dynect_email_Exception('Invalid arguments');
		}

	}

	private function executePost($ch,$reqestedBody,$method,$output)
	{
		if (!is_string($reqestedBody))
		{
			$this->buildPostBody();
		}

		curl_setopt($ch, CURLOPT_POSTFIELDS, $reqestedBody);
		curl_setopt($ch, CURLOPT_POST, 1);

		$this->doExecute($ch, $reqestedBody,$method,$output);
	}

	private function doExecute(&$curlHandle,$reqestedBody,$method,$output)
	{
		$this->setCurlOpts($curlHandle,$method,$output);
		$reqestedBody = curl_exec($curlHandle);


		curl_close($curlHandle);
	}

	private function setCurlOpts(&$curlHandle,$method,$output)
	{
		curl_setopt($curlHandle, CURLOPT_TIMEOUT, 10);
		curl_setopt($curlHandle, CURLOPT_URL, self::END_POINT  . $output . '/' . $method);
		curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curlHandle, CURLOPT_HTTPHEADER, array ('Accept: ' . 'json' ));
	}

  /**
   *
   *
   * @return array|dynect_email_Exception
   */
  public function messages_send($message) {
    $message['to'] = $message['to'][0]['email'];
    return $this->request('send', $message, $http = 'POST');
  }


}

