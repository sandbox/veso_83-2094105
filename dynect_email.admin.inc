<?php
/**
 * @file
 * Administrative forms for dynect_email module.
 */


/**
 * Administrative settings.
 *
 * @return array
 *   An array containing form items to place on the module settings page.
 */
function dynect_email_admin_settings($form, &$form_state) {
  $key = variable_get('dynect_email_api_key');
  $form['dynect_email_api_key'] = array(
    '#title' => t('dynect_email API Key'),
    '#type' => 'textfield',
    '#description' => t('Create or get your API key from the !link.',
      array('!link' => l(t('dynect_email settings'), 'https://email.dynect.net/'))),
    '#default_value' => $key,
  );

  if ($key) {
    $mailsystem_config_keys = mailsystem_get();
    $in_use = FALSE;
    $usage_rows = array();
    foreach ($mailsystem_config_keys as $key => $sys) {
      if ($sys === 'dynect_emailMailSystem' && $key != 'dynect_email_test') {
        $in_use = TRUE;
        $usage_rows[] = array(
          $key,
          $sys,
        );
      }
    }
    if ($in_use) {
      $usage_array = array(
        '#theme' => 'table',
        '#header' => array(
          t('Module Key'),
          t('Mail System'),
        ),
        '#rows' => $usage_rows,
      );
      $form['dynect_email_status'] = array(
        '#type' => 'markup',
        '#markup' => t('DynECT is currently configured to be used by the Module Keys below. To edit the setings or add additional for DynECT, use !link.<br /><br />'
            . drupal_render($usage_array),
          array('!link' => l(t('Mail System'), 'admin/config/system/mailsystem'))),
      );
    }
    elseif (!$form_state['rebuild']) {
      drupal_set_message(t('PLEASE NOTE: DynECT is not configured for use. In order to DynECT, you must configure at least one MailSystemInterface (other than DynECT) to use "DynECTMailSystem" in !link, or you will only be able to send Test Emails through DynECT.',
        array('!link' => l(t('Mail System'), 'admin/config/system/mailsystem'))), 'warning');
    }

    $form['email_options'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#title' => t('Email options'),
    );

    $from = dynect_email_from();
    $form['email_options']['dynect_email_from'] = array(
      '#title' => t('From address'),
      '#type' => 'textfield',
      '#description' => t('This is the sender`s email address, if it`s not verified by <a href="http://dyn.com/">DynECT</a> system, emails won`t be send, to verify this email please go to <a href="http://dyn.com/">DynECT</a>. '),
      '#default_value' => $from['email'],
    );
    $form['email_options']['dynect_email_from_name'] = array(
      '#type' => 'textfield',
      '#title' => t('From name'),
      '#default_value' => $from['name'],
      '#description' => t('This field is optional it can be used to enter the sender`s name.'),
    );
    $formats = filter_formats();
    $options = array('' => t('-- Select --'));
    foreach ($formats as $v => $format) {
      $options[$v] = $format->name;
    }
    $form['email_options']['dynect_email_filter_format'] = array(
      '#type' => 'select',
      '#title' => t('Input format'),
      '#description' => t('If you select this option the input for the body of your email will be formatted with this option. '),
      '#options' => $options,
      '#default_value' => array(variable_get('dynect_email_filter_format', 'full_html')),
    );

    if (isset($form_state['values'])) {
      $show_batch_limit = $form_state['values']['dynect_email_process_async'];
    }
    else {
      $show_batch_limit = dynect_email_process_async();
    }
    if ($show_batch_limit) {
      $form['asynchronous_options']['dynect_email_batch_limit'] = array(
        '#title' => t('Batch size'),
        '#type' => 'textfield',
        '#size' => '12',
        '#description' => t('Number of messages to send in each cron batch. Zero and negative values are not permited.'),
        '#required' => TRUE,
        '#element_validate' => array('element_validate_integer_positive'),
        '#default_value' => variable_get('dynect_email_batch_limit', 50),
      );
    }
  }
  return system_settings_form($form);
}

/**
 * Javascript callback for the dynect_email_admin_settings form.
 *
 * @param array $form
 *   a drupal form
 * @param array $form_state
 *   drupal form_state
 *
 * @return array
 *   a form section
 */
function dynect_email_admin_settings_form_callback($form, &$form_state) {
  return $form['asynchronous_options'];
}

/**
 * Return a form for sending a test email.
 *
 * @param array $form
 * @param array $form_state
 *
 * @return array $form
 */
function dynect_email_test_form($form, &$form_state) {
  drupal_set_title(t('Send test email'));

  $form['dynect_email_test_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Email address to send a test email to'),
    '#default_value' => variable_get('site_mail', ''),
    '#description' => t('Please enter an email address to which you want to send test email.'),
    '#required' => TRUE,
  );
  $form['dynect_email_test_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Test body contents'),
    '#default_value' => t('If you get this email, it means your site is able to use DynECT to send emails.',
      array('!link' => l(t('link'), 'http://www.drupal.org/project/dynect_email'))),
  );

  $form['test_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send test email'),
  );
  $form['test_cancel'] = array(
    '#type' => 'link',
    '#href' => 'admin/config/services/dynect_email',
    '#title' => t('Cancel'),
  );

  return $form;
}

/**
 * Submit handler for dynect_email_test_form(), sends the test email.
 *
 * @param string $form
 * @param string $form_state
 *
 * @return void
 */
function dynect_email_test_form_submit($form, &$form_state) {
  // If an address was given, send a test email message.
  $test_address = $form_state['values']['dynect_email_test_address'];
  global $language;
  $params['subject'] = t('Drupal dynect_email test email');
  $params['body'] = $form_state['values']['dynect_email_test_body'];
  $mailsystem = mailsystem_get();
  // Check for empty mailsystem config for dynect_email:
  if (empty($mailsystem['dynect_email_test'])) {
    drupal_set_message(t('Automatically set DynECT tests to go through DynECT API: DynECTMailSystem was not previously configured in Mail System.'));
    mailsystem_set(array('dynect_email_test' => 'dynect_emailMailSystem'));
  }
  // Check for wrong mailsystem config for dynect_email, if not empty, and issue a warning:
  elseif ($mailsystem['dynect_email_test'] != 'dynect_emailMailSystem') {
    drupal_set_message(
      t('Mail System is set to send DynECT test emails through %system, not dynect_email. To send test emails using DynECT, go to !link and change the settings.',
        array(
          '%system' => $mailsystem['dynect_email_test'],
          '!link' => l(t('dynect_email'), 'https://email.dynect.net/'))),
      'warning');
    // Hack because we are apparently formatting the body differently than default drupal messages
    $params['body'] = array('0' => $params['body']);
  }
  $result = drupal_mail('dynect_email', 'test', $test_address, $language, $params);
  if (isset($result['result']) && $result['result'] == 'true') {

    drupal_set_message(t('Your message has been sent.', array('%from' => $result['from'], '%to' => $result['to'])), 'status');
  }
}
